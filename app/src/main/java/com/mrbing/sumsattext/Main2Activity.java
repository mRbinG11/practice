package com.mrbing.sumsattext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toast.makeText(getApplicationContext(),"Suck It!",Toast.LENGTH_SHORT).show();
        TextView t=findViewById(R.id.textView);
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
            t.setText(extras.getString("txt"));
    }
}
